#include "fractol.h"

static inline int	is_in(t_fract *f, int i, int j)
{
	int			iter;
	t_complex	z;
	t_complex	c;
	double		tmp;

	tmp = 0;
	iter = 0;
	z.r = (double)(f->n == JULIA ? (i / f->zx + f->x1) : 0);
	z.i = (double)(f->n == JULIA ? (j / f->zy + f->y1) : 0);
	c.r = (double)(f->n == JULIA ? f->julia_cr : i / f->zx + f->x1);
	c.i = (double)(f->n == JULIA ? f->julia_ci : j / f->zy + f->y1);
	while (iter < f->max_iter)
	{
		tmp = z.r;
		z.r = z.r * z.r - z.i * z.i + c.r;
		z.i = 2 * tmp * z.i + c.i;
		if (!(z.r * z.r + z.i * z.i < 4))
			break ;
		iter++;
	}
	if (iter == f->max_iter)
		return (0);
	return (iter);
}

static void			*thread_routine(void *ti)
{
	t_threads_i	*t;
	int			iter;
	int			color;
	int			i;
	int			j;

	iter = 0;
	t = (t_threads_i*)ti;
	i = t->xstart;
	while (i < t->xend)
	{
		j = t->ystart;
		while (j < t->yend)
		{
			iter = is_in(t->mlx->f, i, j);
			color = (iter ? set_color(iter, t->mlx) : t->mlx->f->bcolor);
			img_pix(t->mlx, i, j, color);
			j++;
		}
		i++;
	}
	pthread_exit(NULL);
}

static void			timeit(t_mlx_datas *mlx, int way)
{
	static struct timeval	tval_b;
	struct timeval			tval_a;
	struct timeval			tval_r;

	if (!way)
		gettimeofday(&tval_b, NULL);
	else
	{
		gettimeofday(&tval_a, NULL);
		timersub(&tval_a, &tval_b, &tval_r);
		mlx->f->slast = (long int)tval_r.tv_sec;
		mlx->f->uslast = (long int)tval_r.tv_usec;
	}
}

static void			launchjoin_threads(t_mlx_datas *mlx)
{
	t_threads_i		*tis;
	pthread_attr_t	tattr;
	void			*status;
	int				b;

	b = 0;
	if (!(tis = ft_memalloc(sizeof(t_threads_i) * (unsigned long)mlx->threads)))
		return ;
	if (pthread_attr_init(&tattr))
		fexit("failed to initialise threads attributes", 3);
	while (b < mlx->threads)
	{
		tis[b].mlx = mlx;
		tis[b].xstart = 0;
		tis[b].xend = mlx->sx;
		tis[b].ystart = b * (mlx->sy / mlx->threads);
		tis[b].yend = (b + 1) * (mlx->sy / mlx->threads);
		if (pthread_create(&tis[b].tid, &tattr, &thread_routine, &tis[b]))
			fexit("failed to launch thread", 3);
		b++;
	}
	pthread_attr_destroy(&tattr);
	while (b--)
		if (pthread_join(tis[b].tid, &status))
			ft_printf("mandelbrot(): failed to join thread %d\n", b);
}

void				*jul_man(void *m)
{
	t_mlx_datas		*mlx;

	mlx = (t_mlx_datas*)m;
	timeit(mlx, 0);
	mlx->f->zx = mlx->sx / (mlx->f->x2 - mlx->f->x1) * mlx->f->z;
	mlx->f->zy = mlx->sy / (mlx->f->y2 - mlx->f->y1) * mlx->f->z;
	launchjoin_threads(mlx);
	timeit(mlx, 1);
	mlx_put_image_to_window(mlx->mlx, mlx->mlx_w, mlx->f->mlx_image, 0, 0);
	//if (mlx->infos)
	//	printinfos(mlx);
	if (pthread_mutex_unlock(mlx->f->main_thread_used))
	{
		ft_printf("jul_man(): failed to unlock main_thread mutex\n");
		exit(EXIT_FAILURE);
	}
	return (NULL);
}
