#include "fractol.h"

static void	printinfos_julia(t_mlx_datas *m)
{
	char	*tmp;

	mlx_string_put(m->mlx, m->mlx_w, 10, 70, 0xFFFFFF, "Mouse step : ");
	tmp = ft_itoa(1000 * m->f->julia_mod);
	mlx_string_put(m->mlx, m->mlx_w, 90, 70, 0xFFFFFF, tmp);
	free(tmp);
}

void		printinfos_dimensions(t_mlx_datas *m)
{
	char	**tmp;

	tmp = ft_strtnew(4);
	tmp[0] = ft_itoa(m->f->x1 * 100);
	tmp[1] = ft_itoa(m->f->x2 * 100);
	tmp[2] = ft_itoa(m->f->y1 * 100);
	tmp[3] = ft_itoa(m->f->y2 * 100);
	mlx_string_put(m->mlx, m->mlx_w, 10, 85, 0xFFFFFF, "Area : ");
	mlx_string_put(m->mlx, m->mlx_w, 10, 100, 0xFFFFFF, tmp[0]);
	mlx_string_put(m->mlx, m->mlx_w, 10, 115, 0xFFFFFF, tmp[1]);
	mlx_string_put(m->mlx, m->mlx_w, 10, 130, 0xFFFFFF, tmp[2]);
	mlx_string_put(m->mlx, m->mlx_w, 10, 145, 0xFFFFFF, tmp[3]);
	ft_strtdel(&tmp);
}

void		printinfos(t_mlx_datas *m)
{
	char	**tmp;

	tmp = ft_strtnew(4);
	mlx_string_put(m->mlx, m->mlx_w, 10, 10, 0xFFFFFF, m->fname);
	mlx_string_put(m->mlx, m->mlx_w, 10, 25, 0xFFFFFF, "Iterations : ");
	mlx_string_put(m->mlx, m->mlx_w, 10, 40, 0xFFFFFF, "Threads    : ");
	mlx_string_put(m->mlx, m->mlx_w, 10, 55, 0xFFFFFF, "Time       : ");
	tmp[0] = ft_itoa(m->f->max_iter);
	mlx_string_put(m->mlx, m->mlx_w, 83, 25, 0xFFFFFF, tmp[0]);
	tmp[1] = ft_itoa(m->threads);
	mlx_string_put(m->mlx, m->mlx_w, 83, 40, 0xFFFFFF, tmp[1]);
	tmp[2] = ft_itoa(m->f->slast);
	mlx_string_put(m->mlx, m->mlx_w, 83, 55, 0xFFFFFF, tmp[2]);
	mlx_string_put(m->mlx, m->mlx_w, 88, 55, 0xFFFFFF, ".");
	tmp[3] = ft_itoa(m->f->uslast);
	mlx_string_put(m->mlx, m->mlx_w, 93, 55, 0xFFFFFF, tmp[3]);
	ft_strtdel(&tmp);
	//printinfos_dimensions(m);
	if (m->fractal_no == JULIA)
		printinfos_julia(m);
}

inline int	set_color(int iter, t_mlx_datas *mlx)
{
	int color;

	//color = mlx->f->bcolor | (iter * mlx->f->bcolor / mlx->f->max_iter);
	//color = mlx->f->bcolor * sin(iter) / mlx->f->max_iter;
	color = mlx->f->bcolor & (iter * mlx->f->bcolor / mlx->f->max_iter);
	//color = mlx->f->bcolor & (sin(iter) / mlx->f->max_iter;
	return (color);
}

int			keys_change_color(int key, t_mlx_datas *mlx)
{
	if (key == K_C)
		mlx->f->bcolor = ((rand() * BCOLOR) << 16) | ((rand() * BCOLOR) << 8)\
						| (rand() * BCOLOR);
	else if (key == K_E)
		mlx->f->bcolor = (((rand() * BCOLOR) << 16) & RED_F)
			| (((rand() * BCOLOR) << 8) & GREEN_F)\
			| ((rand() * BCOLOR) & BLUE_F);
	else if (key == K_D)
		mlx->f->bcolor = ((((mlx->f->bcolor >> 16) * BCOLOR) << 16) & RED_F)\
						| ((((mlx->f->bcolor >> 8) * BCOLOR) << 8) & GREEN_F)\
						| (((mlx->f->bcolor) * BCOLOR) & BLUE_F);
	else if (key == K_T)
		mlx->f->bcolor |= (0xee << 24) & 0xff000000;
	else
		return (0);
	return (1);
}
