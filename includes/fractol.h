#ifndef FRACTOL_H
# define FRACTOL_H

# include <sys/time.h>
# include <pthread.h>
# include <math.h>
# ifdef __linux__
#  include <X11/X.h>
# elif __APPLE__
#  include "/System/Library/Frameworks/Tk.framework/Versions/8.5/Headers/X11/X.h"
# endif
# include "libft.h"
# include "mlx.h"

# include "portaudio.h"

# define BCOLOR		0x000000ff
# define RED_F		0x00ff0000
# define GREEN_F	0x0000ff00
# define BLUE_F		0x000000ff

# define MLEFT		0
# define MRIGHT		1
# define MTOP		2
# define MBOTTOM	3

# define MANDEL_X1	-2.1
# define MANDEL_X2	0.6
# define MANDEL_Y1	-1.2
# define MANDEL_Y2	1.2
# define JULIA_X1	-1.5
# define JULIA_X2	1.5
# define JULIA_Y1	-1.5
# define JULIA_Y2	1.5
# define JULIA_CR	0.285
# define JULIA_CI	0.01

# define DEFAULT_WINSIZE	900
# define DEFAULT_THREADSNUM	4
# define USAGE				"Usage:\nfractol [-tiIxy] mandelbrot|julia|cantor"

# ifdef __linux__
#  define K_ESC	65307
#  define K_ENT	65293
#  define K_PLUS	61
#  define K_LESS	45
#  define K_INFO	105
#  define K_ARLE 65361
#  define K_ARUP 65362
#  define K_ARRI 65363
#  define K_ARDO 65364
#  define K_PUP	65365
#  define K_PDO	65366
#  define K_Z	122
#  define K_S	115
#  define K_SPA	32
#  define KM_ZI	5
#  define KM_ZO	4
#  define KM_LE	1
#  define K_C	99
#  define K_R	114
#  define K_G	103
#  define K_B	98
#  define IS_ARROW(x) ((x >= K_ARLE) && (x <= K_ARDO))
# elif __APPLE__
#  define K_ESC	53
#  define K_ENT	36
#  define K_PLUS	26
#  define K_LESS	28
#  define K_INFO 2
#  define K_ARLE 123
#  define K_ARRI 124
#  define K_ARDO 125
#  define K_ARUP 126
#  define K_PUP	116
#  define K_PDO	121
#  define K_Z	33
#  define K_S	40
#  define K_SPA	49
#  define KM_ZI	5
#  define KM_ZO	4
#  define KM_LE	1
#  define K_R	37
#  define K_G	43
#  define K_B	12
#  define K_C	4
#  define K_E	3
#  define K_D	34
#  define K_T	38
#  define IS_ARROW(x) ((x >= K_ARLE) && (x <= K_ARUP))
# endif

# define MANDEL	0
# define JULIA	1
# define CANTOR	2
# define BUDDHA	3

typedef struct		s_complex
{
	double			r;
	double			i;
}					t_complex;

typedef struct		s_pixelt
{
	int				**screen;
	int				(*setcolor)(int, int, int);
	int				(*addcolor)(int, int, int);
}					t_pixelt;

typedef struct		s_frac
{
	void			*mlx_image;
	pthread_mutex_t	*main_thread_used;
	char			*img;
	pthread_t		main_thread;
	int				max_iter;
	int				iter_step;
	int				bcolor;
	int				n;
	double			dec[2];
	double			z;
	double			zx;
	double			zy;
	double			x1;
	double			x2;
	double			y1;
	double			y2;
	double			julia_cr;
	double			julia_ci;
	double			julia_mod;
	long int		slast;
	long int		uslast;
	t_pixelt		px;
}					t_fract;

typedef struct		s_mlx_datas
{
	void			*mlx;
	void			*mlx_w;
	char			*fname;
	t_fract			*f;
	int				sx;
	int				sy;
	int				bpp;
	int				bpl;
	int				ed;
	int				fractal_no;
	int				threads;
	int				infos;
}					t_mlx_datas;

typedef struct		s_threads_i
{
	t_mlx_datas		*mlx;
	pthread_mutex_t	*mut;
	int				**pixels;
	pthread_t		tid;
	int				xstart;
	int				xend;
	int				ystart;
	int				yend;
}					t_threads_i;

int					f_mlx_init(t_mlx_datas *mlx);
int					img_pix(t_mlx_datas *mlx, int x, int y, int color);
int					handle_keys(int key, void *m);
int					handle_mouse(int button, int x, int y, void *m);
int					handle_motion(int x, int y, void *m);
int					handle_expose(void *m);
int					keys_change_color(int key, t_mlx_datas *mlx);
int					fractal(t_mlx_datas *mlx);
void				*jul_man(void *m);
void				*buddhabrot(void *m);
void				*cantor(void *m);
void				printinfos(t_mlx_datas *m);
int					set_color(int iter, t_mlx_datas *mlx);
int					zoom(t_mlx_datas *mlx, int in);
int					bzoom(t_mlx_datas *mlx, int x, int y);
void				init_cantor(t_mlx_datas *mlx);
void				init_julia(t_mlx_datas *mlx);
void				init_mandel(t_mlx_datas *mlx);
void				init_default(t_mlx_datas *mlx);
int					change_fractal(t_mlx_datas *mlx);
void				fexit(char *msg, int status);
int					move_display(t_mlx_datas *mlx, int way);
void				audioget(t_mlx_datas *mlx);
PaErrorCode			close_portaudio(PaStream *stream);
#endif
