#include "fractol.h"

void	*cantor(void *m)
{
	t_mlx_datas *mlx;
	int			x;
	int			y;

	x = 0;
	mlx = (t_mlx_datas*)m;
	while (x < mlx->sx)
	{
		y = 0;
		while (y < mlx->sy)
		{
			if (x & y)
				img_pix(mlx, x, y, 0x00ff00);
			y++;
		}
		x++;
	}
	mlx_put_image_to_window(mlx->mlx, mlx->mlx_w, mlx->f->mlx_image, 0, 0);
	if (pthread_mutex_unlock(mlx->f->main_thread_used))
	{
		ft_printf("cantor(): failed to unlock main_thread mutex\n");
		exit(EXIT_FAILURE);
	}
	return (NULL);
}
