#include "fractol.h"

int			handle_expose(void *m)
{
	t_mlx_datas *mlx;

	ft_printf("expose\n");
	mlx = (t_mlx_datas*)m;
	if (pthread_mutex_trylock(mlx->f->main_thread_used))
		return (0);
	mlx_put_image_to_window(mlx->mlx, mlx->mlx_w, mlx->f->mlx_image, 0, 0);
	if (pthread_mutex_unlock(mlx->f->main_thread_used))
		fexit("handle_expose(): failed to unlock main_thread mutex\n",\
				EXIT_FAILURE);
	return (0);
}

static int	handle_others_keys(int key, t_mlx_datas *mlx)
{
	if (key == K_PLUS || key == K_LESS)
	{
		if (!(key == K_LESS && (mlx->f->max_iter - mlx->f->iter_step <= 0)))
			mlx->f->max_iter += mlx->f->iter_step * (key == K_LESS ? -1 : 1);
	}
	else if (key == K_PUP || key == K_PDO)
		mlx->f->julia_mod += 0.0005 * (key == K_PUP ? 1 : -1);
	else if (key == K_Z || key == K_S)
		zoom(mlx, (key == K_Z ? 1 : 0));
	else if (key == K_SPA)
		change_fractal(mlx);
	else if (IS_ARROW(key))
		move_display(mlx, key);
	else if (keys_change_color(key, mlx))
		return (0);
	else
		return (1);
	return (0);
}

int			handle_keys(int key, void *m)
{
	t_mlx_datas *mlx;

	mlx = (t_mlx_datas*)m;
	if (key == K_ESC)
		fexit("esc pressed, exiting", EXIT_SUCCESS);
	else if (key == K_ENT)
	{
		if (mlx->fractal_no != JULIA)
		{
			ft_printf("Audio 'input' is only available with Julia fractal\n");
			return (0);
		}
		audioget(mlx);
	}
	if (pthread_mutex_trylock(mlx->f->main_thread_used))
		return (0);
	else if (handle_others_keys(key, mlx))
	{
		ft_printf("unkown key: %d\n", key);
		pthread_mutex_unlock(mlx->f->main_thread_used);
		return (0);
	}
	fractal(mlx);
	return (0);
}

int			handle_mouse(int button, int x, int y, void *m)
{
	t_mlx_datas *mlx;

	mlx = (t_mlx_datas*)m;
	if (pthread_mutex_trylock(mlx->f->main_thread_used))
		return (0);
	if (button == KM_ZI)
		zoom(mlx, 1);
	else if (button == KM_ZO)
		zoom(mlx, 0);
	else if (button == KM_LE)
		bzoom(mlx, x, y);
	else
		ft_printf("mouse: %d at (%d,%d)\n", button, x, y);
	fractal(mlx);
	return (0);
}

int			handle_motion(int x, int y, void *m)
{
	static int	sx = 0;
	static int	sy = 0;
	t_mlx_datas	*mlx;

	mlx = (t_mlx_datas*)m;
	if (mlx->fractal_no != JULIA)
		return (0);
	if (pthread_mutex_trylock(mlx->f->main_thread_used))
		return (0);
	if (sx > x)
		mlx->f->julia_cr += mlx->f->julia_mod;
	else
		mlx->f->julia_cr -= mlx->f->julia_mod;
	if (sy > y)
		mlx->f->julia_ci += mlx->f->julia_mod;
	else
		mlx->f->julia_ci -= mlx->f->julia_mod;
	fractal(mlx);
	sx = x;
	sy = y;
	return (0);
}
