#include "fractol.h"

static inline void	set_zc(t_fract *f, t_complex *z, t_complex *c, \
		double zx, double zy, int i, int j)
{
	c->r = i / zx + f->x1;
	c->i = j / zy + f->y1;
	z->r = 0;
	z->i = 0;
}

static inline int	**is_in(t_fract *f, int i, int j, \
		t_threads_i *t)
{
	int			iter;
	t_complex	z;
	t_complex	c;
	double		tmp;
	int			(*tmp_pixels)[2];
	int count = 0;
	int	col_iter[3] = {1000, 2000, 9000};

	tmp_pixels = ft_memalloc(sizeof(int[2]) * f->max_iter);
	if (!tmp_pixels)
		fexit("can't malloc for tmp_pixels", 3);

	tmp = 0;
	iter = 0;
	set_zc(f, &z, &c, f->zx, f->zx, i, j);
	while (iter < f->max_iter)
	{
		tmp = z.r;
		z.r = z.r * z.r - z.i * z.i + c.r;
		z.i = 2 * tmp * z.i + c.i;
		tmp_pixels[iter][0] = (z.r - f->x1) * 250;
		tmp_pixels[iter][1] = (z.i - f->y1) * 250;
		if (!(z.r * z.r + z.i * z.i < 4))
			break ;
		iter++;
	}
	if (iter == f->max_iter)
		return (NULL);
	count = 0;
	while (count < iter)
	{
		if (!(pthread_mutex_lock(t->mut)))
		{
			if (tmp_pixels[count][0] >= 0 && tmp_pixels[count][0] < t->mlx->sx \
					&& tmp_pixels[count][1] >= 0 && tmp_pixels[count][1] < t->mlx->sy)
			{
				if (iter < col_iter[0] && count < col_iter[0])
					t->pixels[tmp_pixels[count][0]][tmp_pixels[count][1]] |= RED_F & ((t->pixels[tmp_pixels[count][0]][tmp_pixels[count][1]] & RED_F) + (1 << 16));
				if (iter < col_iter[1] && count < col_iter[1])
					t->pixels[tmp_pixels[count][0]][tmp_pixels[count][1]] |= GREEN_F & ((t->pixels[tmp_pixels[count][0]][tmp_pixels[count][1]] & GREEN_F) + (1 << 8));
				if (iter < col_iter[2] && count < col_iter[2])
					t->pixels[tmp_pixels[count][0]][tmp_pixels[count][1]] |= BLUE_F & ((t->pixels[tmp_pixels[count][0]][tmp_pixels[count][1]] & BLUE_F) + 1);
			}
			count++;
			pthread_mutex_unlock(t->mut);
		}
	}
	free(tmp_pixels);
	return (NULL);
}

static inline void	pixcolor(t_mlx_datas *mlx, t_threads_i *ti)
{
	int i = 0;
	int	j;
	int	color = 0;

	while (i < mlx->sx)
	{
		j = 0;
		while (j < mlx->sy)
		{
			color = ti->pixels[i][j];
			ti->pixels[i][j] = 0;
			img_pix(mlx, i, j, color);
			j++;
		}
		i++;
	}
}

static void	*thread_routine(void *ti)
{
	t_threads_i	*t;
	int			i;
	int			j;

	t = (t_threads_i*)ti;
	i = t->xstart;
	t->mlx->f->zx = t->mlx->sx / (t->mlx->f->x2 - t->mlx->f->x1) * t->mlx->f->z;
	t->mlx->f->zy = t->mlx->sy / (t->mlx->f->y2 - t->mlx->f->y1) * t->mlx->f->z;
	while (i < t->xend)
	{
		j = t->ystart;

		while (j < t->yend)
		{
			is_in(t->mlx->f, i, j, t);
			j++;
		}
		i++;
	}
	return (NULL);
}

static void	timeit(t_mlx_datas *mlx, int way)
{
	static struct timeval tval_b, tval_a, tval_r;

	if (!way)
		gettimeofday(&tval_b, NULL);
	else
	{
		gettimeofday(&tval_a, NULL);
		timersub(&tval_a, &tval_b, &tval_r);
		mlx->f->slast = (long int)tval_r.tv_sec;
		mlx->f->uslast = (long int)tval_r.tv_usec;
	}
}

void		*buddhabrot(void *m)
{
	t_mlx_datas		*mlx;
	t_threads_i		*tis;
	pthread_attr_t	tattr;
	int				b;
	void			*status;
	pthread_mutex_t	mut;
	static int **pixels = NULL; int i = 0;

	if (pthread_mutex_init(&mut, NULL))
		fexit("failed to initialize mutex\n", 4);
	mlx = (t_mlx_datas*)m;
	if (!pixels) {
		pixels = ft_memalloc(sizeof(int*) * mlx->sx);
		if (!pixels)
			fexit("cant' mallox for pixels", 3);
		i = 0;
		while (i < (mlx->sx))
		{
			pixels[i] = ft_memalloc(sizeof(int) * mlx->sy);
			if (!pixels[i])
				fexit("cant' mallox for pixels[x]", 3);
			i++;
		}
		ft_printf("all is alloc'd\n");
	}
	b = 0;
	timeit(mlx, 0);
	if (!(tis = ft_memalloc(sizeof(t_threads_i) * mlx->threads)))
		return (NULL);
	if (pthread_attr_init(&tattr))
		fexit("failed to initialise threads attributes", 3);
	while (b < mlx->threads)
	{
		tis[b].mlx = mlx;
		tis[b].mut = &mut;
		tis[b].pixels = pixels;
		tis[b].xstart = 0;
		tis[b].xend = mlx->sx;
		tis[b].ystart = b * (mlx->sy / mlx->threads);
		tis[b].yend = (b+1) * (mlx->sy / mlx->threads);
		if (pthread_create(&tis[b].tid, &tattr, &thread_routine, &tis[b]))
			fexit("failed to launch thread", 3);
		b++;
	}
	pthread_attr_destroy(&tattr);
	while (b--)
		if (pthread_join(tis[b].tid, &status))
			ft_printf("mandelbrot(): failed to join thread %d\n", b);
	//if (mlx->infos)
		//printinfos(mlx);
	pixcolor(mlx, tis);
	mlx_put_image_to_window(mlx->mlx, mlx->mlx_w, mlx->f->mlx_image, 0, 0);
	timeit(mlx, 1);
	free(tis);
	if (pthread_mutex_unlock(mlx->f->main_thread_used))
	{
		ft_printf("unlock main_thread mutex failed\n");
		exit (EXIT_FAILURE);
	}
	ft_printf("done! %ld.%lds\n", mlx->f->slast, mlx->f->uslast);
	return (NULL);
}
