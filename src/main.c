#include "fractol.h"

void		fexit(char *msg, int status)
{
	ft_printf("%s\n", msg);
	close_portaudio(NULL);
	exit(status);
}

static int	putopt(char *opt_name, char *opt_value, t_mlx_datas *mlx)
{
	int	ov;

	if (!opt_name[1] || !opt_value)
	{
		ft_printf("bad parameter : '%s %s\n", opt_name, opt_value);
		return (1);
	}
	if (!(ov = ft_atoi(opt_value)))
		return (1);
	if (opt_name[1] == 't')
		mlx->threads = ov;
	else if (opt_name[1] == 'x')
		mlx->sx = ov;
	else if (opt_name[1] == 'y')
		mlx->sy = ov;
	else if (opt_name[1] == 'I')
		mlx->f->max_iter = ov;
	else if (opt_name[1] == 'i')
		mlx->f->iter_step = ov;
	return (0);
}

static int	parse_params(int ac, char **av, t_mlx_datas *mlx)
{
	int i;

	i = 1;
	if (ac < 2)
		fexit(USAGE, 1);
	init_default(mlx);
	while (i < ac - 1)
	{
		if (av[i][0] == '-')
			if (putopt(av[i], av[i + 1], mlx))
				fexit(USAGE, 1);
		i++;
	}
	mlx->fname = av[i];
	if (!ft_strcmp(av[i], "mandelbrot") || !ft_strcmp(av[i], "buddhabrot"))
		init_mandel(mlx);
	else if (!ft_strcmp(av[i], "julia"))
		init_julia(mlx);
	else if (!ft_strcmp(av[i], "cantor"))
		init_cantor(mlx);
	else
		fexit(USAGE, 1);
	return (0);
}

int			main(int ac, char **av)
{
	t_mlx_datas	*mlx;

	mlx = ft_memalloc(sizeof(t_mlx_datas));
	if (!mlx)
		return (EXIT_FAILURE);
	mlx->f = ft_memalloc(sizeof(t_fract));
	if (!mlx->f)
		return (EXIT_FAILURE);
	if (parse_params(ac, av, mlx))
		return (EXIT_FAILURE);
	if (f_mlx_init(mlx))
		return (EXIT_FAILURE);
	fractal(mlx);
	mlx_loop(mlx->mlx);
	return (EXIT_FAILURE);
}
