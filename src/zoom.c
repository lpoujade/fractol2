#include "fractol.h"

int			zoom(t_mlx_datas *mlx, int in)
{
	if (in)
	{
		mlx->f->x1 -= 0.1 * (mlx->f->x1 - mlx->f->x2);
		mlx->f->y1 -= 0.1 * (mlx->f->y1 - mlx->f->y2);
		mlx->f->x2 += 0.1 * (mlx->f->x1 - mlx->f->x2);
		mlx->f->y2 += 0.1 * (mlx->f->y1 - mlx->f->y2);
		mlx->f->max_iter += 1;
	}
	else
	{
		mlx->f->x1 += 0.1 * (mlx->f->x1 - mlx->f->x2);
		mlx->f->y1 += 0.1 * (mlx->f->y1 - mlx->f->y2);
		mlx->f->x2 -= 0.1 * (mlx->f->x1 - mlx->f->x2);
		mlx->f->y2 -= 0.1 * (mlx->f->y1 - mlx->f->y2);
		mlx->f->max_iter -= 1;
	}
	return (0);
}

int			move_display(t_mlx_datas *mlx, int way)
{
	double	r;

	r = (mlx->f->y2 / mlx->f->y1) * (mlx->f->x2 / mlx->f->x1);
	if (way == K_ARDO)
	{
		mlx->f->y2 -= r * 0.01;
		mlx->f->y1 -= r * 0.01;
	}
	else if (way == K_ARUP)
	{
		mlx->f->y1 += r * 0.01;
		mlx->f->y2 += r * 0.01;
	}
	else if (way == K_ARRI)
	{
		mlx->f->x1 -= r * 0.01;
		mlx->f->x2 -= r * 0.01;
	}
	else if (way == K_ARLE)
	{
		mlx->f->x2 += r * 0.01;
		mlx->f->x1 += r * 0.01;
	}
	return (0);
}

int			bzoom(t_mlx_datas *mlx, int x, int y)
{
	static double s = 0.5;

	if (x < (mlx->sx / 2))
	{
		mlx->f->x1 += mlx->f->x1 * 0.01;
	}
	else
	{
		mlx->f->x2 += mlx->f->x2 * 0.01;
	}
	if (y < (mlx->sy / 2))
	{
		mlx->f->y1 += mlx->f->y1 * 0.01;
	}
	else
	{
		mlx->f->y2 += mlx->f->y2 * 0.01;
	}
	s += 0.5;
	return (0);
}
