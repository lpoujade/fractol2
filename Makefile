# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lpoujade <lpoujade@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/02/11 13:41:06 by lpoujade          #+#    #+#              #
#    Updated: 2018/06/05 19:02:37 by lpoujade         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME=fractol
SRCDIR=./src
OBJDIR=./.obj

CC=gcc
CPPFLAGS=-Iincludes/ -Ilibft/includes -Iminilibx/
CFLAGS=-Wall -Werror -Wextra -g

LDFLAGS=-Llibft/ -Lminilibx
LDLIBS=-lft -lmlx -lm -lpthread

uname_S = $(shell sh -c uname -s 2>/dev/null || echo not)
ifeq ($(uname_S), Linux)
	CC=gcc
	LDLIBS+=-lXext -lX11
	CPPFLAGS+= -I/usr/include/mlx/
	echo=echo -e
endif
ifeq ($(uname_S), Darwin)
	CC=clang
	LDLIBS+=-framework OpenGL -framework AppKit
	LDLIBS+=-framework CoreAudio -framework AudioToolbox -framework AudioUnit -framework CoreServices -framework Carbon
	#CFLAGS+=-Weverything -Wno-documentation -Wno-missing-noreturn -Wno-strict-prototypes -Wno-sign-conversion -Wno-float-conversion -Wno-cast-qual -Wno-double-promotion
	echo=echo
endif

LIB=libft/libft.a

SRC=main.c draw.c libix.c handlers.c utils.c jul_man.c cantor.c fract_init.c \
	buddhabrot.c zoom.c audioget.c
OBJ=$(SRC:.c=.o)

SRCS=$(addprefix $(SRCDIR)/,$(SRC))
OBJS=$(addprefix $(OBJDIR)/,$(OBJ))

all: $(LIB) $(NAME)

run: all
	./fractol mandelbrot

$(NAME): $(OBJS)
	@$(CC) $^ -o $@ libportaudio.a $(LDFLAGS) $(LDLIBS) && $(echo) "linking to\033[32m" $@ "\033[0m"

$(OBJDIR)/%.o: $(SRCDIR)/%.c includes/fractol.h
	@mkdir -p $(OBJDIR)
	@$(CC) $(CFLAGS) $(CPPFLAGS) -c $< -o $@ && $(echo) "compiling\033[36m" $@ "\033[0m"

$(LIB):
	make -C libft/

clean:
	-@rm $(OBJS) && $(echo) "deleting" $(OBJS)

fclean: clean
	-@rm $(NAME) && $(echo) "deleting" $(NAME)

re: fclean all

.PHONY: all, clean, fclean, re
