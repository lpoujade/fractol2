/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpoujade <lpoujade@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/05 15:38:22 by lpoujade          #+#    #+#             */
/*   Updated: 2018/06/04 16:00:09 by lpoujade         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int	fractal(t_mlx_datas *mlx)
{
	pthread_attr_t	tattr;

	if (!pthread_mutex_trylock(mlx->f->main_thread_used))
		ft_printf("fractal(): mutex was not locked\n");
	if (pthread_attr_init(&tattr))
		ft_printf("fractal(): error initializing main thread attributes\n");
	if (mlx->fractal_no != BUDDHA)
	{
		if (pthread_create(&mlx->f->main_thread, &tattr, \
					(mlx->fractal_no == CANTOR ? &cantor : &jul_man), mlx))
			ft_printf("fractal(): error launching main thread\n");
	}
	else
	{
		if (pthread_create(&mlx->f->main_thread, &tattr, \
					&buddhabrot, mlx))
			ft_printf("fractal(): error launching main thread\n");
	}
	pthread_attr_destroy(&tattr);
	if (pthread_detach(mlx->f->main_thread))
		ft_printf("fractal(): error detaching main thread\n");
	return (0);
}

int	change_fractal(t_mlx_datas *mlx)
{
	if (mlx->fractal_no == BUDDHA)
		mlx->fractal_no = MANDEL;
	else
		mlx->fractal_no++;
	//init_default(mlx);
	if (mlx->fractal_no == MANDEL || mlx->fractal_no == BUDDHA)
		init_mandel(mlx);
	else if (mlx->fractal_no == JULIA)
		init_julia(mlx);
	else if (mlx->fractal_no == CANTOR)
		init_cantor(mlx);
	return (0);
}
