#include "fractol.h"

void	init_default(t_mlx_datas *mlx)
{
	mlx->sx = DEFAULT_WINSIZE;
	mlx->sy = DEFAULT_WINSIZE;
	mlx->threads = DEFAULT_THREADSNUM;
	mlx->infos = 1;
	mlx->f->main_thread_used = ft_memalloc(sizeof(pthread_mutex_t));
	if (!mlx->f->main_thread_used)
	{
		ft_printf("init_default(): failed to allocate main_thread mutex\n");
		exit(EXIT_FAILURE);
	}
	if (pthread_mutex_init(mlx->f->main_thread_used, NULL))
	{
		ft_printf("init_default(): failed to init main_thread mutex\n");
		exit(EXIT_FAILURE);
	}
	mlx->f->max_iter = 100;
	mlx->f->iter_step = 1;
	mlx->f->dec[0] = 1.0;
	mlx->f->dec[1] = 1.0;
	mlx->f->bcolor = BCOLOR;
}

void	init_mandel(t_mlx_datas *mlx)
{
	if (!ft_strncmp("mandel", mlx->fname, ft_strlen("mandel") - 1))
		mlx->fractal_no = MANDEL;
	else
		mlx->fractal_no = BUDDHA;
	mlx->f->n = mlx->fractal_no;
	mlx->f->x1 = MANDEL_X1;
	mlx->f->x2 = MANDEL_X2;
	mlx->f->y1 = MANDEL_Y1;
	mlx->f->y2 = MANDEL_Y2;
}

void	init_julia(t_mlx_datas *mlx)
{
	mlx->fractal_no = JULIA;
	mlx->f->n = JULIA;
	mlx->f->x1 = -1.5;
	mlx->f->x2 = 1.5;
	mlx->f->y1 = -1.5;
	mlx->f->y2 = 1.5;
	mlx->f->julia_cr = JULIA_CR;
	mlx->f->julia_ci = JULIA_CI;
	mlx->f->julia_mod = 0.00005;
}

void	init_cantor(t_mlx_datas *mlx)
{
	mlx->fractal_no = CANTOR;
	mlx->f->n = CANTOR;
	mlx->f->iter_step = 50;
}
