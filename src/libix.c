#include "fractol.h"

int	f_mlx_init(t_mlx_datas *mlx)
{
	mlx->mlx = mlx_init();
	if (!mlx->mlx)
		return (EXIT_FAILURE);
	mlx->mlx_w = mlx_new_window(mlx->mlx, mlx->sx, mlx->sy, "fract'ol");
	if (!mlx->mlx_w)
		return (EXIT_FAILURE);
	mlx->f->mlx_image = mlx_new_image(mlx->mlx, mlx->sx, mlx->sy);
	if (!mlx->f->mlx_image)
		return (EXIT_FAILURE);
	mlx->f->img = mlx_get_data_addr(mlx->f->mlx_image, \
			&mlx->bpp, &mlx->bpl, &mlx->ed);
	mlx_mouse_hook(mlx->mlx_w, &handle_mouse, mlx);
	mlx_expose_hook(mlx->mlx_w, &handle_expose, mlx);
	mlx_hook(mlx->mlx_w, KeyPress, KeyPressMask, &handle_keys, mlx);
	mlx_hook(mlx->mlx_w, MotionNotify, PointerMotionMask, &handle_motion, mlx);
	mlx->f->z = 1;
	return (0);
}

int	img_pix(t_mlx_datas *mlx, int x, int y, int color)
{
	int		i;
	char	*img;

	img = mlx->f->img;
	i = x * (mlx->bpp / 8) + y * mlx->bpl;
	if (mlx->ed)
	{
		img[i] = (char)((color >> 24));
		img[i + 1] = (char)((color >> 16) & 0x000000ff);
		img[i + 2] = (char)((color >> 8) & 0x000000ff);
		img[i + 3] = (char)(color & 0x000000ff);
	}
	else
	{
		img[i + 3] = (char)((color >> 24));
		img[i + 2] = (char)((color >> 16) & 0x000000ff);
		img[i + 1] = (char)((color >> 8) & 0x000000ff);
		img[i] = (char)(color & 0x000000ff);
	}
	return (0);
}
