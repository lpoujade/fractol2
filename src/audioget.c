#include "fractol.h"

static inline float	getmean(const void *inp_buf, unsigned long frame_count)
{
	unsigned long	frame;
	float			mean;
	float			*inb;

	mean = 0;
	frame = 0;
	inb = (float*)inp_buf;
	while (frame < frame_count)
	{
		mean += inb[frame];
		frame++;
	}
	mean /= frame_count;
	return (mean);
}

static int			audio_callback(const void *input, void *output,
		unsigned long frame_count, const PaStreamCallbackTimeInfo *time_info,
		PaStreamCallbackFlags status_flags, void *m)
{
	static float	oldmean = 0;
	t_mlx_datas		*mlx;
	float			mean;

	(void)output;
	(void)time_info;
	(void)status_flags;
	mlx = (t_mlx_datas*)m;
	mean = getmean(input, frame_count);
	if (!pthread_mutex_trylock(mlx->f->main_thread_used))
	{
		if (mean > oldmean)
		{
			mlx->f->julia_cr += mlx->f->julia_mod * mean * 10;
			mlx->f->julia_ci += mlx->f->julia_mod * mean * 10;
		}
		else if (mean < oldmean)
		{
			mlx->f->julia_cr -= mlx->f->julia_mod * oldmean * 10;
			mlx->f->julia_ci -= mlx->f->julia_mod * oldmean * 10;
		}
		fractal(mlx);
	}
	oldmean = mean;
	return (0);
}

static PaErrorCode	init_portaudio(PaStream *stream, t_mlx_datas *mlx)
{
	int err;

	if ((err = Pa_Initialize()) != paNoError)
	{
		ft_printf("failed to initialize PortAudio: %s\n", Pa_GetErrorText(err));
		return (err);
	}
	err = Pa_OpenDefaultStream(&stream, 1, 0, paFloat32, 44100,
			paFramesPerBufferUnspecified, audio_callback, mlx);
	if (err != paNoError)
	{
		ft_printf("failed to open default stream: %s\n", Pa_GetErrorText(err));
		return (err);
	}
	err = Pa_StartStream(stream);
	if (err != paNoError)
	{
		ft_printf("failed to start stream : %s\n", Pa_GetErrorText(err));
		return (err);
	}
	close_portaudio(stream);
	return (paNoError);
}

PaErrorCode			close_portaudio(PaStream *stream)
{
	static PaStream		*save_stream;
	PaErrorCode			err;

	if (stream)
	{
		save_stream = stream;
		return (paNoError);
	}
	if (!save_stream)
		return (0);
	if ((err = Pa_StopStream(save_stream)) != paNoError)
	{
		ft_printf("failed to stop stream: %s\n", Pa_GetErrorText(err));
		return (err);
	}
	if ((err = Pa_Terminate()) != paNoError)
	{
		ft_printf("failed to close PortAudio: %s\n", Pa_GetErrorText(err));
		return (err);
	}
	ft_printf("PortAudio terminated\n");
	return (paNoError);
}

void				audioget(t_mlx_datas *mlx)
{
	static int		pa_initialized = 0;
	static PaStream	*stream;

	if (!pa_initialized)
	{
		if (init_portaudio(&stream, mlx) != paNoError)
			return ;
		pa_initialized = 1;
		ft_printf("PortAudio is initialized\n");
	}
	else
	{
		close_portaudio(NULL);
		pa_initialized = 0;
		stream = NULL;
		ft_printf("PortAudio closed.\n");
	}
}
